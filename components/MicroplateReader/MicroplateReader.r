library( componentSkeleton )
library( xlsx )

expand <- function( plate.measurements, plate.layout ) {
  Measurements <- unlist( plate.measurements )
  # Test length of measurements file is same as layout sheets
  if( length( Measurements ) != length( plate.layout[[1]] ) ) {
    write.error(cf, 'Measurements file have different length than annotations file')
    return(1)
  }
  # Assume wells will always be numbered in the format A1, A2, ..., P24 in the 384-well plate
  Wells <- unlist( lapply( 1:ncol( plate.measurements ), function(x) paste( row.names( plate.measurements ), x , sep="" ) ) )
  # Concatenate well id, measurements and the labels from the layout annotation file
  return( data.frame( Wells, Measurements, plate.layout ) )
}

read.xlsx.table <- function( file, sheetIndex, row.names ){
  xlsx.data <- read.xlsx( file, sheetIndex = sheetIndex, header = F, stringsAsFactors = F )
  col.names.idx <- grep( "Raw Data", xlsx.data[,2] ) + 1
  start.idx <- col.names.idx + 1
  measurement <- xlsx.data[start.idx:dim(xlsx.data)[1], 2:dim(xlsx.data)[2]]
  rownames(measurement) <- xlsx.data[start.idx:dim(xlsx.data)[1],row.names]
  colnames(measurement) <- xlsx.data[col.names.idx,2:dim(xlsx.data)[2]]
  
  return(measurement)
}

execute <- function( cf ) {
  # Read INPUT ports: XLSX folder as list of data frames
  folder <- get.input( cf, 'in' )
  filenames <- list.files( folder, pattern = "*.xlsx", full.names = TRUE )
  measurements <- lapply( filenames, read.xlsx.table, sheetIndex = 1, row.names = 1 )
  names( measurements ) <- filenames

  # Read INPUT ports: XLSX annotation
  wb <- loadWorkbook( get.input( cf, 'annotations' ) )
  sheets <- getSheets( wb )
  sheet.names <- names( sheets )
  layout.sheets <- lapply( sheet.names, function( sheet ) read.xlsx( get.input( cf, 'annotations' ), sheetName = sheet, header = T, row.names = 1 ) )
  # Flatten data frames into a vector by column
  layout <- lapply( layout.sheets, function( x ) as.vector( as.matrix( x ) ) )
  names( layout ) <- sheet.names
  # Test length of all sheets is the same
  if( length( unique( lapply( layout, length ) ) ) > 1 ) {
    write.error(cf, 'Sheets in annotations file have different length')
    return(1)
  }

  # List of dataframes. Expand each plate to one long table with annotations
  result <- lapply( measurements, expand, plate.layout = layout )

  # Write list of data frames as Anduril array
  array.out.dir <- get.output(cf, 'out')
  if (!file.exists(array.out.dir)) {
      dir.create(array.out.dir, recursive=TRUE)
  }

  # Write output array
  array.out.object <- Array.new()
  for(i in 1:length( result )) {
    key = gsub('.xlsx$','',basename( filenames[i] ) )
    filename = paste(key, ".csv", sep="")
    CSV.write(paste(array.out.dir, "/", filename, sep=""), result[[i]])
    array.out.object <- Array.add(array.out.object, key, filename)
  }
  Array.write(cf, array.out.object, 'out')
  return(0)
}

main(execute)
