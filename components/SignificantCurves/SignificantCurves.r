library(componentSkeleton)

makeOutDir <- function(cf, dir.name){
  dir <- get.output(cf, dir.name)
  if (!file.exists(dir)) {
    dir.create(dir, recursive=TRUE)
  }
  return(dir)
}

execute <- function(cf) {
  # Read INPUT ports:
  # CSV array as list of data frames obtained from the DSRTplots component
  score.files <- Array.read( cf, 'in' )
  cell.line <- Array.getKey(score.files, 1)
  cell.line <- strsplit(cell.line, split="_")[[1]][1]
  cat("------- Cell Line", toupper(cell.line), "-------\n\n")
  
  # Read paramenters: score threshold to select significant drugs
  score.thr <- get.parameter( cf, 'scoreThr' )
  
  # Prepare output folders
  out.dir <- makeOutDir(cf, 'out')
  plot.dir <- makeOutDir(cf, 'plots')
  auc.dir <- makeOutDir(cf, 'auc')
  array.out.object <- Array.new()
  
  for (i in 1:Array.size(score.files)){
    
    ## Get array key and assign filename
    key <- Array.getKey(score.files, i)
    
    full.name <- toupper(paste(strsplit(key, split="_")[[1]], collapse=" - "))
    cat("------- Sample", full.name, "-------\n\n")
    
    # Read measurements
    cat("Reading", key, "data ...\n\n")
    scores <- CSV.read(Array.getFile(score.files, key))
    
    # Filter data using absolute score threshold
    cat("Filtering", key, "data using an absolute score threshold of", score.thr, "...\n\n")
    filtered.scores <- scores[scores$Absolute_Scores >= score.thr, ]
    
    filename <- file.path(out.dir, paste(key, "significant_drug_scores.csv", sep="_"))
    CSV.write(filename, filtered.scores)
    array.out.object <- Array.add(array.out.object, key, filename)
    
    if (dim(filtered.scores)[1] > 0){
      # Prepare data to plot
      cat("Preparing", key, "data for plotting", score.thr, "...\n\n")
      sorted.scores <- filtered.scores[order(filtered.scores$Scores),]
      sorted <- sorted.scores$Scores
      names(sorted) <- as.character(sorted.scores$Drugs)
      
      # Plot data
      cat("Plotting", key, "significant scores ...\n\n")
      plot.filename <- paste(key, "drug_scores", sep="_")
      png.open(file.path(plot.dir, paste(plot.filename, ".png", sep="")), 1200, 800)
      cols <- c("blue", "red")[(sorted > 0) + 1]  
      
      ## Pass the colors in to barplot()
      par(mar=c(10, 4, 4, 2) + 0.1)
      barplot(sorted, col = cols, main = paste(gsub("_", " - ", toupper(key)), "Drug Scores", sep=" "),
              las=2, ylab = "Scores")
      png.close()
    } 
    
    # Plot area under the curve
    cat("Plotting", key, " area under the curve ...\n\n")
    auc.cols <- colnames(scores)[grep("AUC", colnames(scores))]
    auc <- scores[,auc.cols]
    index <- order(auc[,grep("RTXDXR", auc.cols)])
    auc <- auc[index,]
    auc <- t(auc)
    colnames(auc) <- scores$Drugs[index]
    rownames(auc) <- unlist(lapply(strsplit(auc.cols, split="_"), function(x) x[2]))
    plot.filename <- paste(key, "auc", sep="_")
    png.open(file.path(auc.dir, paste(plot.filename, ".png", sep="")), 1200, 800)
    barplot(auc, main=gsub("_", " - ", toupper(plot.filename)), col=c("darkblue","red"), beside=TRUE,las=2)
    legend("topleft", 
           legend = rownames(auc), 
           fill = c("darkblue","red"), ncol = 1,
           cex = 1,
           text.font=2)
    png.close()
  }
  
  Array.write(cf, array.out.object, 'out')
  return(0)
}

main(execute)