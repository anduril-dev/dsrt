library(componentSkeleton)

execute <- function(cf) {
  # Read INPUT ports: CSV array as list of data frames obtained from the MicroplateReader component
  array <- Array.read( cf, 'in' )
  pos.name <- get.parameter( cf, 'pos' )
  neg.name <- get.parameter( cf, 'neg' )
  
  plot.dir <- get.output(cf, 'plots')
  if (!file.exists(plot.dir)) {
    dir.create(plot.dir, recursive=TRUE)
  }
  
  plate.statistics <- list()
  for (i in 1:Array.size(array)){
    
    # Get array key
    key <- Array.getKey(array, i)
    
    # Read measurements
    cat("Reading", key, "data ...\n\n")
    data.table <- CSV.read(Array.getFile(array, key))
                             # sep = "\t", header=T, stringsAsFactors = F)
    
    #Compute plate statistics
    cat("Computing plate statistics on", key, "data ...\n\n")
    plate.statistics[[key]] <- cbind(data.frame(Key=key), compute.plate.statistics(data.table, plot.dir, key))
  }
  
  # Write plate statistics results
  cat("Print statistics table\n\n")
  table.out <- do.call(rbind, plate.statistics)
  CSV.write(get.output(cf, 'out'), table.out)
  return(0)
}

main(execute)
