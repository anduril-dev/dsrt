library(componentSkeleton)
library(plyr)
library(reshape2)
library(gplots)
library(plotrix)
library(ggplot2)
library(fields)

makeOutDir <- function(cf, dir.name){
  dir <- get.output(cf, dir.name)
  if (!file.exists(dir)) {
    dir.create(dir, recursive=TRUE)
  }
  return(dir)
}

plotCurves <- function(data, dir, drug, vary.drug, key, y.range){
  if (drug == "Doxorubicin"){
    x.labels <- paste("R", 0:7, sep="")
  } else if (drug == "Rituximab"){
    x.labels <- paste("D", 0:7, sep="")
  }
  
  # Plot each curve separately
  for (id in names(data)){
    cat("  - Plotting", id, "...\n")
    png.open(paste(dir, drug, id, "Dose_Response.png", sep="_"))
      plot(0:7, unlist(data[[id]]["Normalized.Mean"]), xlab=paste(vary.drug, "Concentration Index", sep=" "),
           ylab="Percentage Inhibition", type="o", col = "red", ylim = y.range, xaxt = "n",
           main=paste(key, id, sep=" - "))
      axis(1, at=0:(length(data)-1), labels= x.labels, las= 0)
    png.close()
  }
  
  # Plot all curves together
  cat("  - Plotting all", drug, "curves...\n")
  png.open(paste(dir, drug, "Dose_Responses.png", sep="_"),1300,700)
    par(mfrow=c(2,4))
    for (id in names(data)){
      plot(0:7, unlist(data[[id]]["Normalized.Mean"]), xlab=paste(vary.drug, "Concentration Index", sep=" "),
           ylab="Percentage Inhibition", type="o", col = "red", ylim = y.range, xaxt = "n",
           main=paste(key, id, sep=" - "))
      axis(1, at=0:(length(data)-1), labels= x.labels, las= 0)
    }
  png.close()
}


execute <- function(cf) {
  # Read INPUT ports: CSV array as list of data frames obtained from the MicroplateReader component
  array <- Array.read( cf, 'in' )
  
  # Read paramenters: names of positive and negative controls
  pos.name <- get.parameter( cf, 'pos' )
  neg.name <- get.parameter( cf, 'neg' )
  
  # Prepare output folders
  out.dir <- makeOutDir(cf, 'out')
  matrix.dir <- makeOutDir(cf, 'matrices')
  plot.dir <- makeOutDir(cf, 'plots')
  
  for (i in 1:Array.size(array)){
    
    # Get array key
    key <- Array.getKey(array, i)
    filename.out  <- file.path( out.dir, key )
    filename.matrix  <- file.path( matrix.dir, key )
    filename.plot  <- file.path( plot.dir, key )
    
    # Read measurements
    cat("\n------- Sample", key, "-------\n\n")
    cat("Reading", key, "data ...\n\n")
    data.table <- CSV.read(Array.getFile(array, key))
    
    # Summarize measurements based on combination group
    cat("Summarizing", key, "data based on combination group ...\n\n")
    summary.table <- ddply(data.table, .(Rituximab, Doxorubicin, Controls), summarize, Mean=mean(Measurements),
                           SD=sd(Measurements), CV=sd(Measurements)/mean(Measurements)*100)
    
    # Normalize data
    cat("Normalizing", key, "data ...\n\n")
    neg <- summary.table$Mean[which(summary.table$Controls == neg.name)]
    pos <- summary.table$Mean[which(summary.table$Controls == pos.name)]
    summary.table$Normalized.Mean <- ((neg-summary.table$Mean)/(neg-pos))*100
    
    CSV.write(paste(filename.out, "_Combination_Matrix_Results.csv",sep=""), summary.table)
    
    # Create matrix of Rituximab vs. Doxorubicin
    cat("Creating", key, "matrix of Rituximab vs Doxorubicin ...\n\n")
    summary.table <- summary.table[!is.na(summary.table$Rituximab),]
    mat <- acast(summary.table, Doxorubicin~Rituximab, value.var="Normalized.Mean")

    cat("Plotting", key, "data ...\n")
    # Plot combination matrix
    cat("- Plotting", key, "combination matrix of Rituximab vs Doxorubicin ...\n")
    range <- c(min(0, min(summary.table$Normalized.Mean)), max(100, max(summary.table$Normalized.Mean)))
    
    png.open(paste(filename.matrix, "_Combination_Matrix.pngv",sep=""))
      par(mar=c(5,4.5,4,7))
      image(mat, xaxt="n", yaxt="n", col=colorpanel(128, low='white', mid='orange', high='blue'),
            xlab="Doxorubicin", ylab="Rituximab", main=paste("Combination Matrix - ", key, sep=""), axes = F,
            zlim = range)
      axis(2, at=seq(0,1,length.out=ncol(mat)), labels= colnames(mat), las= 2)
      axis(1, at=seq(0,1,length.out=nrow(mat)), labels= rownames(mat), las= 0)
      image.plot(mat, zlim = range, legend.only=T, col=colorpanel(128, low='white', mid='orange', high='blue'))
    png.close()
    
    # Plot Rituximan curves
    cat("- Plotting", key, "Rituximab curves ...\n")
    summary.table.R <- split(summary.table, summary.table$Rituximab)
    plotCurves(summary.table.R, filename.plot, "Rituximab", "Doxorubicin", key, range)
    
    # Plot Doxorubicin curves
    cat("- Plotting", key, "Doxorubicin curves ...\n")
    summary.table.D <- split(summary.table, summary.table$Doxorubicin)
    plotCurves(summary.table.D, filename.plot, "Doxorubicin", "Rituximab", key, range)
  }
  
  return(0)
}

main(execute)
