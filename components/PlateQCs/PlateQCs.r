library(componentSkeleton)
library(stringr)
library(plotrix)

remove.borders <- function (data.table, borders, columns.to.remove=NA, rows.to.remove=NA){
  ids.to.remove <- c()
  
  if (borders != "rows"){
    columns.ids <- as.numeric(str_extract(data.table$Wells, "[0-9]+"))
    ids.to.remove <- c(ids.to.remove, which(columns.ids %in% columns.to.remove))
  }
  
  if (borders != "columns"){
    rows.ids <- str_extract(data.table$Wells, "[aA-zZ]+")
    ids.to.remove <- c(ids.to.remove, which(rows.ids %in% rows.to.remove))
  }
  
  border.removed <- data.table[-ids.to.remove, ]
  
  return(border.removed)
}

compute.statistics <- function(data.table){
  
  res <- t(as.matrix(vector(mode = "double", length = 6)))
  colnames(res) <- c("mean", "var", "std", "cv", "min", "max")
  
  measurements <- data.table$Measurements
  
  if(length(is.na(measurements)) == 0){
    measurements <- measurements[-which(is.na(measurements))]
  }
  
  res[1,"mean"] <- round(mean(measurements), digits = 2)
  res[1,"var"] <- round(var(measurements), digits = 2)
  res[1,"std"] <- round(sd(measurements), digits = 2)
  res[1,"cv"] <- round((res[1,"std"]*100/ res[1,"mean"]), digits = 2)
  res[1,"min"] <- min(measurements)
  res[1,"max"] <- max(measurements)
  
  return(as.data.frame(res, stringsAsFactors=F))
}

compute.plate.statistics <- function(data.table, out.dir, key ){
  
  # Get plate dimensions
  n.columns <- length(unique(as.numeric(str_extract(data.table$Wells, "[0-9]+"))))
  n.rows <- length(unique(str_extract(data.table$Wells, "[aA-zZ]+")))
  
  # Remove controls
  data.to.plot <- data.table
  if (length(grep("Controls", colnames(data.table))) > 0){
    cat(" - Remove controls...\n")
    data.table <- data.table[is.na(data.table$Controls),]
  }
  
  # Compute statistics
  cat(" - Compute statistics on the whole plate...\n")
  whole.plate.statistics <- compute.statistics(data.table)
  colnames(whole.plate.statistics) <- c("Mean" , "Variance", "Standard_Deviation",
                                        "CV", "Minimum", "Maximum")
  whole.plate.matrix <- matrix(data.to.plot$Measurements, byrow=F, ncol=24, nrow=16)
  
  # Plot colored matrix
  cat(" - Plot colored matrix of the whole plate...\n")
  filename  <- file.path( out.dir, key )
  png.open( paste0(filename, '.png') )
    color2D.matplot(whole.plate.matrix,c(0,1),c(1,0),c(0,0),extremes=NA,cellcolors=NA,show.legend=TRUE,nslices=10,xlab="",
                    ylab="",do.hex=FALSE,axes=TRUE,show.values=TRUE,vcol="white",vcex=1,
                    border="black",na.color=NA, main=paste("Plate Survival Assay for", toupper(key), sep=" "))
  png.close()
  
  # Remove first and last columns
  cat(" - Remove columns 1 and 24 and compute statistics on the remaining wells...\n")
  data.table.no.border.columns <- remove.borders(data.table, borders = "columns", columns.to.remove = c(1, n.columns))
  data.to.plot.no.border.columns <- remove.borders(data.to.plot, borders = "columns", columns.to.remove = c(1, n.columns))
  
  # Compute statistics
  no.border.columns.statistics <- compute.statistics(data.table.no.border.columns)
  colnames(no.border.columns.statistics) <- c("Mean_NoBorderColumns" , "Variance_NoBorderColumns", "Standard_Deviation_NoBorderColumns",
                                              "CV_NoBorderColumns", "Minimum_NoBorderColumns", "Maximum_NoBorderColumns")
  no.border.columns.matrix <- matrix(data.to.plot.no.border.columns$Measurements, byrow=F, ncol=22, nrow=16)
  
  # Plot colored matrix
  cat(" - Plot colored matrix of the plate without external columns...\n")
  filename  <- file.path( out.dir, key )
  png.open( paste0(filename, '_no_columns.png') )
    color2D.matplot(no.border.columns.matrix,c(0,1),c(1,0),c(0,0),extremes=NA,cellcolors=NA,show.legend=TRUE,nslices=10,xlab="",
                    ylab="",do.hex=FALSE,axes=TRUE,show.values=TRUE,vcol="white",vcex=1,
                    border="black",na.color=NA, main=paste("Plate Survival Assay for", toupper(key), "- No External Columns", sep=" "))
  png.close()
  
  # Remove first and last row
  cat(" - Remove external borders and compute statistics on the remaining wells...\n")
  data.table.no.borders <- remove.borders(data.table, borders = "both",
                                          rows.to.remove = c("A", toupper(letters[n.rows])),
                                          columns.to.remove = c(1, n.columns))
  
  data.to.plot.no.borders <- remove.borders(data.to.plot, borders = "both",
                                          rows.to.remove = c("A", toupper(letters[n.rows])),
                                          columns.to.remove = c(1, n.columns))
  # Compute statistics
  no.border.statistics <- compute.statistics(data.table.no.borders)
  colnames(no.border.statistics) <- c("Mean_NoBorders" , "Variance_NoBorders", "Standard_Deviation_NoBorders",
                                      "CV_NoBorders", "Minimum_NoBorders", "Maximum_NoBorders")
  no.borders.matrix <- matrix(data.to.plot.no.borders$Measurements, byrow=F, ncol=22, nrow=14)
  
  # Plot colored matrix
  cat(" - Plot colored matrix of the plate without external borders...\n")
  filename  <- file.path( out.dir, key )
  png.open( paste0(filename, '_no_borders.png') )
    color2D.matplot(no.borders.matrix,c(0,1),c(1,0),c(0,0),extremes=NA,cellcolors=NA,show.legend=TRUE,nslices=10,xlab="",
                    ylab="",do.hex=FALSE,axes=TRUE,show.values=TRUE,vcol="white",vcex=1,
                    border="black",na.color=NA, main=paste("Plate Survival Assay for", toupper(key), "- No External Borders", sep=" "))
  png.close()
  
  cat(" - Combine statistics...\n")
  plate.statistics <- cbind(whole.plate.statistics, no.border.columns.statistics,
                            no.border.statistics)
  
  cat(" - Return statistics...\n\n")
  return(plate.statistics)
}

execute <- function(cf) {
  # Read INPUT ports: CSV array as list of data frames obtained from the MicroplateReader component
  array <- Array.read( cf, 'in' )
  
  plot.dir <- get.output(cf, 'plots')
  if (!file.exists(plot.dir)) {
    dir.create(plot.dir, recursive=TRUE)
  }
  
  plate.statistics <- list()
  for (i in 1:Array.size(array)){
    
    # Get array key
    key <- Array.getKey(array, i)
    
    # Read measurements
    cat("Reading", key, "data ...\n\n")
    data.table <- CSV.read(Array.getFile(array, key))
                             
    #Compute plate statistics
    cat("Computing plate statistics on", key, "data ...\n\n")
    plate.statistics[[key]] <- cbind(data.frame(Key=key), compute.plate.statistics(data.table, plot.dir, key))
  }
  
  # Write plate statistics results
  cat("Print statistics table\n\n")
  table.out <- do.call(rbind, plate.statistics)
  CSV.write(get.output(cf, 'out'), table.out)
  return(0)
}

main(execute)
