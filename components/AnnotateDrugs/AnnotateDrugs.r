library(componentSkeleton)
library(plyr)
library(reshape2)
library(gplots)
library(plotrix)
library(ggplot2)
library(gridExtra)

execute <- function(cf) {
  ## Read INPUT ports: CSV array as list of data frames obtained from the MicroplateReader component
  array <- Array.read( cf, 'in' )

  ## Read annotation CSV file containing drug concentrations
  concentrations <- CSV.read( get.input( cf, 'annotations' ) )

  ## Initialize output array
  array.out.dir <- get.output(cf, 'out')
  if (!file.exists(array.out.dir)) {
    dir.create(array.out.dir, recursive=TRUE)
  }
  array.out.object <- Array.new()
  
  annotated.data <- list()
  for (i in 1:Array.size(array)){
    
    ## Get array key and assign filename
    key <- Array.getKey(array, i)
    filename <- paste(key, ".csv", sep="")
    
    ## Read measurements
    cat("------- Annotating", key, "sample -------\n\n")
    cat("Reading", key, "data ...\n\n")
    data.table <- CSV.read(Array.getFile(array, key))
    
    y.name <- colnames(concentrations)[grep("Well", colnames(concentrations))]
    annotated <- merge(data.table, concentrations, by.x = "Wells", by.y = y.name)
    
    annotated.data[[key]] <-annotated[with(annotated, order(DCol, DRow)), ]
    
    CSV.write(file.path(array.out.dir, filename), annotated.data[[key]])
    array.out.object <- Array.add(array.out.object, key, filename)
  }
  Array.write(cf, array.out.object, 'out')
  
  return(0)
}

main(execute)
