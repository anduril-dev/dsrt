library(componentSkeleton)
library(plyr)
library(reshape2)
library(gplots)
library(plotrix)
library(ggplot2)
library(gridExtra)

execute <- function(cf) {
  ## Read INPUT ports: CSV array as list of data frames obtained from the MicroplateReader component
  array <- Array.read( cf, 'in' )
  
  ## Read paramenters: names of positive and negative controls
  pos.name <- get.parameter( cf, 'pos' )
  neg.name <- get.parameter( cf, 'neg' )
  
  plot.dir <- get.output(cf, 'plots')
  if (!file.exists(plot.dir)) {
    dir.create(plot.dir, recursive=TRUE)
  }
  
  control.statistics <- list()
  for (i in 1:Array.size(array)){
    
    ## Get array key and assign filename
    key <- Array.getKey(array, i)
    filename  <- file.path( plot.dir, key )
    
    #3 Read measurements
    cat("------- Sample", key, "-------\n\n")
    cat("Reading", key, "data ...\n\n")
    data.table <- CSV.read(Array.getFile(array, key))
    original.data <- data.table
    
    ## Positive control measurements
    pos <- data.table$Measurements[which(data.table$Controls == pos.name)]
    ## Nevative control measurements
    neg <- data.table$Measurements[which(data.table$Controls == neg.name)]
    
    ## Compute plate statistics
    cat("Computing control statistics on", key, "data ...\n")
    
    ## Positive controls mean, standard deviation and coefficient of variation
    cat("- Computing positive controls mean, standard deviation and coefficient of variation ...\n")
    pos.mean <- mean(pos)
    pos.std <- sd(pos)
    pos.cv <- (pos.std / pos.mean) * 100
    
    ## Nevative controls mean, standard deviation and coefficient of variation
    cat("- Computing negative controls mean, standard deviation and coefficient of variation ...\n")
    neg.mean <- mean(neg)
    neg.std <- sd(neg)
    neg.cv <- (neg.std / neg.mean) * 100
    
    ## Signal window (or signal to background ratio)
    cat("- Computing signal window ...\n")
    sw <- neg.mean/pos.mean
    
    ## Z-factor value needs to be between >0.5
    cat("- Computing Z factor ...\n")
    z <- 1-((3*neg.std + 3*pos.std)/abs(neg.mean-pos.mean))
    
    ## Strictly Standardized Mean Difference
    cat("- Computing SSMD (Strictly Standardized Mean Difference) ...\n\n")
    ssmd <- abs(neg.mean-pos.mean)/sqrt(var(pos)+var(neg))
    
    ## Identify row and column of each well
    cat("- Identifying row and column coordinates of each well of", key, "plate ...\n\n")
    data.table$Rows <- sapply(data.table$Wells, function(x) substr(x, 1, 1))
    data.table$Columns <- as.numeric(gsub("^.","",data.table$Wells))
    
    ## Mean, standard deviation and coefficient of variation of the first column
    cat("- Computing mean, standard deviation and coefficient of variation of the first column ...\n")
    first.column <- data.table$Measurements[data.table$Columns == "1"]
    first.mean <- mean(first.column)
    first.std <- sd(first.column)
    first.cv <- (first.std / first.mean) * 100
    
    ## Mean, standard deviation and coefficient of variation of the last column
    cat("- Computing mean, standard deviation and coefficient of variation of the last column ...\n")
    last.column <- data.table$Measurements[data.table$Columns == "24"]
    last.mean <- mean(last.column)
    last.std <- sd(last.column)
    last.cv <- (last.std / last.mean) * 100
    
    ## Add statistics to table
    cat("Adding", key, "control statistics to the output table ...\n\n")
    control.statistics[[key]] <- data.frame(Key=key, Positive.Controls.Mean=pos.mean, Negative.Controls.Mean=neg.mean,
                                            First.Column.Mean=first.mean, Last.Column.Mean=last.mean,
                                            Positive.Controls.StDev=pos.std, Negative.Controls.StDev=neg.std,
                                            First.Column.StDev=first.std, Last.Column.StDev=last.std,
                                            Positive.Controls.CV=pos.cv, Negative.Controls.CV=neg.cv,
                                            First.Column.CV=first.cv, Last.Column.CV=last.cv,
                                            Signal.Window= sw, Z.Factor=z, SSMD = ssmd)
    ## Assign class to each well
    cat("Assigning each well of", key, "plate to a class ...\n\n")
    data.table$Class <- "Experiment"
    data.table$Class[grep(pos.name, data.table$Controls)] <- pos.name
    data.table$Class[grep(neg.name, data.table$Controls)] <- neg.name
    data.table$Class[apply(original.data[,-c(1,2)], 1, function(x) sum(is.na(x))) == (dim(original.data)[2]-2)] <- "Cells"
    
    cat("Plotting", key, "data ...\n")
    
    ## Plot data by column
    cat("- Plotting", key, "data by column ...\n")
    plot.by.column <- qplot(data=data.table, x=Columns, y=Measurements, color=Class, geom = c("point", "smooth"),
                            main=paste("Luminescent signals by column", key, sep="\n"))
    
    png.open( paste(filename, "_Scatterplot_by_Column.png", sep=""),800,600)
      print(plot.by.column)
    png.close()
    
    ## Plot controls by column
    cat("- Plotting ", key, "controls by column ...\n")
    control.data <- data.table[grep("Experiment", data.table$Class, invert=T),]
    plot.controls.by.column <- qplot(data=control.data, x=Columns, y=Measurements, color=Class, geom = c("point", "smooth"),
                                     main=paste("Luminescent signals of controls by column", key, sep="\n"))
    
    png.open( paste(filename, "_Scatterplot_of_Controls_by_Column.png", sep=""),800,600)
      print(plot.controls.by.column)
    png.close()
    
    ## Plot by row
    cat("- Plotting ", key, "data by row ...\n")
    plot.by.row <- qplot(data=data.table, x=Rows, y=Measurements, color=Class, geom = c("point", "smooth"),
                         main=paste("Luminescent signals by row", key, sep="\n"))
    png.open( paste(filename, "_Scatterplot_by_Row.png", sep=""),800,600)
      print(plot.by.row)
    png.close()
    
    ## combine plots
    cat("- Combining", key, "plots ...\n\n")
    png.open( paste(filename, "_Scatterplots.png", sep=""),2000,600)
      grid.arrange(plot.by.column, plot.controls.by.column, plot.by.row, ncol=3)
    png.close()
  }
  
  # Write plate statistics results
  cat("Printing statistics table\n\n")
  table.out <- do.call(rbind, control.statistics)
  CSV.write(get.output(cf, 'out'), table.out)
  return(0)
}

main(execute)
