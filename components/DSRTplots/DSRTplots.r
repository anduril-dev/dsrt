library(componentSkeleton)

makeOutDir <- function(cf, dir.name){
  dir <- get.output(cf, dir.name)
  if (!file.exists(dir)) {
    dir.create(dir, recursive=TRUE)
  }
  return(dir)
}

removeColumn <- function(data, colID){
  well.cols <- grep(colID, colnames(data))
  if (sum(apply(data[well.cols], 1, function(x) length(unique(x))) == 1) != dim(data)[1]){
    write.error(cf, paste("Files haven't been combined correctly!", colID, "don't match!", sep=" "))
  } else {
    data <- data[,-well.cols]
  }
  return(data)
}

filterInactive <- function(x, pbs, rtxdxr, drug.col, activity.thr){
  if ( (max(as.numeric(x[,pbs])) <  as.numeric(activity.thr)) && (max(as.numeric(x[,rtxdxr])) <  as.numeric(activity.thr)) ) {
    res <- F
  } else {
    res <- T 
  }
  
  cat("Drug:", x[1,drug.col],  ", Max PBS:", max(as.numeric(x[,pbs])),  ", Max RTX-DXR:",  max(as.numeric(x[,rtxdxr])), ", Active:", res, ", Activity Thr:", as.numeric(activity.thr), "\n")
  
  return(res)
}

plotData <- function(data, out.dir, normalized.dir, dir, cell.line, plot.name, array.out.object, array.normalized.object,
                     score.dir, activity.thr){
  # Define colors to be used
  plot.colors <- c("red", "blue", "green", "brown", "violet", "orange")
  
  # Combine all conditions in one data.frame
  combined.data <- do.call(cbind, data)
  
  # Remove columns containing the same values or values not necessary for the analysis
  cat("Removing columns containing the same values or values not necessary for the analysis ...\n\n")
  combined.data <- removeColumn(combined.data, "Wells")
  combined.data <- removeColumn(combined.data, "Controls")
  measurement.cols <- grep("Measurements", colnames(combined.data))
  combined.data <- combined.data[, -measurement.cols]
  
  # Remove drugs not common to all plates
  cat("Removing drugs not common to all plates ...\n\n")
  drug.cols <- grep("Drugs", colnames(combined.data))
  shared <- which(apply(combined.data[drug.cols], 1, function(x) length(unique(x))) == 1)
  combined.data <- combined.data[shared,]
  
  # Remove unwanted NA wells
  cat("Removing unwanted NA wells ...\n\n")
  unwanted.na <- which(!is.na(combined.data[, drug.cols[1]])) 
  combined.data <- combined.data[unwanted.na,]
  
  # Make a unique drug column
  cat("Making a unique drug column ...\n\n")
  combined.data <- combined.data[, -drug.cols[-1]]
  colnames(combined.data)[drug.cols[1]] <- "Drugs"
  
  # Remove drugs with different concentrations
  cat("Removing drugs with different concentrations ...\n\n")
  conc.cols <- grep("Concentration", colnames(combined.data))
  shared <- which(apply(combined.data[conc.cols], 1, function(x) length(unique(x))) == 1)
  combined.data <- combined.data[shared,]
  
  # Make a unique concentration column
  cat("Making a unique concentration columns ...\n\n")
  combined.data <- combined.data[, -conc.cols[-1]]
  colnames(combined.data)[conc.cols[1]] <- "Concentration"
  
  # Make a unique drug class column
  cat("Making a unique drug class column ...\n\n")
  effect.cols <- grep("Effect", colnames(combined.data))
  combined.data <- combined.data[, -effect.cols[-1]]
  colnames(combined.data)[effect.cols[1]] <- "Effect"
  
  # Write normalized data
  cat("Writing normalized data ...\n\n")
  filename <- file.path(normalized.dir, paste(plot.name, "normalized_measurements.csv", sep="_"))
  data.to.write <- combined.data[order(combined.data$Drugs, combined.data$Concentration),]
  CSV.write(filename, data.to.write)
  array.normalized.object <- Array.add(array.normalized.object, plot.name, filename)
  
  # Split data by drug
  cat("Split data by drug ...\n\n")
  combined.data$Drugs <- factor(combined.data$Drugs)
  data.by.drugs <- split(combined.data, combined.data$Drugs)
  
  normalized.cols <- colnames(combined.data)[grep("Normalized", colnames(combined.data))]
  n.lines <- length(normalized.cols)
  
  pbs <- normalized.cols[grep("pbs", normalized.cols)]
  rtxdxr <- normalized.cols[grep("rtxdxr", normalized.cols)]
  drug.col <- colnames(combined.data)[grep("Drugs", colnames(combined.data))]
    
  # Filter out inactive drugs
  cat("Filtering out inactive drugs having less than", activity.thr, "% inhibition ...\n\n")
  if (n.lines == 2){
    active <- which(unlist(lapply(data.by.drugs, function(x) filterInactive(x, pbs, rtxdxr, drug.col, activity.thr))))
  } else {
    active <- 1:length(data.by.drugs)
  }
 
  filtered.data.by.drug <- data.by.drugs[active]
  cat("Before:", length(data.by.drugs),"After:", length(active),"\n\n")
  
  drugs <- names(data.by.drugs)
  filtered.drugs <- names(filtered.data.by.drug)
    
  # Initialiaze curve scoring matrix
  scores <- data.frame(Drugs = filtered.drugs, Effect = rep(NA, length(filtered.drugs)),
                       Absolute_Scores = rep(NA, length(filtered.drugs)), Scores = rep(NA, length(filtered.drugs)),
                       AUC_PBS = rep(NA, length(filtered.drugs)), AUC_RTXDXR = rep(NA, length(filtered.drugs)),
                       stringsAsFactors = F)
  
  cat("Plotting drugs for", gsub("_", " - ", toupper(plot.name)), "...\n")
  d <- 0
  zero.line <- rep(0, 5)
  for (drug in drugs){
    
    # Sorting measurements based on concentration
    cat("- Sorting data ...\n")
    sorted <- order(data.by.drugs[[drug]]$Concentration, decreasing = F)
    sorted.data <- data.by.drugs[[drug]][sorted,]
    
    if (drug %in% scores$Drugs){
      d <- d+1
      
      scores$Effect[d] <- filtered.data.by.drug[[drug]]$Effect[1]
      
      # Computing the geometric area between curves
      if (n.lines == 2){
        cat("- Computing scores ...\n")
        f.score <- approxfun(log10(sorted.data$Concentration), sorted.data[,rtxdxr]-sorted.data[,pbs])     # piecewise linear function
        f.abs.score <- function(x) abs(f.score(x))                 # take the positive value
        scores$Scores[d] <-  integrate(f.score, log10(sorted.data$Concentration)[1], log10(sorted.data$Concentration)[5])$value
        scores$Absolute_Scores[d] <-  integrate(f.abs.score, log10(sorted.data$Concentration)[1],
                                                log10(sorted.data$Concentration)[5])$value
        
        f.pbs.auc <- approxfun(log10(sorted.data$Concentration), sorted.data[,pbs] - zero.line)
        f.rtxdxr.auc <- approxfun(log10(sorted.data$Concentration), sorted.data[,rtxdxr]-zero.line)
        scores$AUC_PBS[d] <-  integrate(f.pbs.auc, log10(sorted.data$Concentration)[1], log10(sorted.data$Concentration)[5])$value
        scores$AUC_RTXDXR[d] <-  integrate(f.rtxdxr.auc, log10(sorted.data$Concentration)[1],
                                           log10(sorted.data$Concentration)[5])$value
      }
    }
    
    # Condition IDs
    ids <- strsplit(normalized.cols, split="\\.")
    for(i in 1:length(ids)){
      ids[[i]] <- paste(ids[[i]][-length(ids[[i]])], collapse=" ")
      ids[[i]] <- toupper(paste(strsplit(ids[[i]], split="_")[[1]][2:3], collapse=" "))
    }
    ids <- unlist(ids)
    
    # Plotting each drug
    cat("- Plotting", drug, "...\n")
    plot.filename <- paste(plot.name, drug, sep="_")
    # png.open(file.path(dir, paste(plot.filename, ".png", sep="")))
    png(file.path(dir, paste(plot.filename, ".png", sep="")))
    plot(log10(sorted.data$Concentration), sorted.data[,normalized.cols[1]],
         type="l", col=plot.colors[1], main=paste(gsub("_", " - ", toupper(plot.filename)), sorted.data$Effect[1], sep=" - "),
         xlab = "Drug Concentrations", ylab = "Inhibition Percentage", ylim=range(-50:130), lwd=2)
    for(col in 2:n.lines){
      lines(log10(sorted.data$Concentration),sorted.data[,normalized.cols[col]],
            col=plot.colors[col], lwd=2)
    }
    abline(100, 0, col = "lightgray")
    abline(0, 0, col = "lightgray")
    legend("topright", # places a legend at the appropriate place
           ids, # puts text in the legend
           lty=c(1,1), # gives the legend appropriate symbols (lines)
           lwd=c(2.5,2.5), col=plot.colors[1:n.lines])
    # png.close()
    dev.off()
  }
  
  if (n.lines == 2){
    filename <- file.path(out.dir, paste(plot.name, "drug_scores.csv", sep="_"))
    CSV.write(filename, scores)
    array.out.object <- Array.add(array.out.object, plot.name, filename)
    
    sorted.scores <- scores[order(scores$Absolute_Scores),]
    sorted <- sorted.scores$Absolute_Scores
    names(sorted) <- as.character(sorted.scores$Drugs)
    
    plot.filename <- paste(plot.name, "absolute_drug_scores", sep="_")
    png.open(file.path(score.dir, paste(plot.filename, ".png", sep="")), 1200, 800)
    par(mar=c(10, 4, 4, 2) + 0.1)
    barplot(sorted, col = "red", main = paste(gsub("_", " - ", toupper(plot.name)), "- Absolute Drug Scores", sep=" "),
            las=2, ylab = "Scores")
    png.close()
  }
  
  return(list(out = array.out.object, normalized = array.normalized.object))
}

execute <- function(cf) {
  # Read INPUT ports:
  # CSV of data frames obtained from the MicroplateReader component
  dsrt.samples <- CSV.read(get.input( cf, 'in' ))
  cell.line <- strsplit(as.character(dsrt.samples$Key), split="_")[[1]][1]
  cat("------- Cell Line", toupper(cell.line), "-------\n\n")
  
  # Divide samples by day
  days <- unlist(lapply(strsplit(as.character(dsrt.samples$Key), split="_"), function(x) x[3]))
  by.day <- dsrt.samples
  by.day$Days <- days
  by.day <- split(by.day, by.day$Days)
  
  # Annotation file containing all drugs used in the experiment and the classes they belong to.
  classes <- read.csv(get.input( cf, 'annotations' ))
  
  # Read paramenters: names of positive and negative controls
  pos.name <- get.parameter( cf, 'pos' )
  neg.name <- get.parameter( cf, 'neg' )
  combine <- get.parameter( cf, 'combine' )
  activity.thr <- get.parameter( cf, 'activityThr' )
  
  # Prepare output folders
  out.dir <- makeOutDir(cf, 'out')
  normalized.dir <- makeOutDir(cf, 'normalized')
  plot.dir <- makeOutDir(cf, 'plots')
  combined.dir <- makeOutDir(cf, 'combined')
  score.dir <- makeOutDir(cf, 'scores')
  array.object <- list(out = Array.new(), normalized = Array.new())
  
  sample <- list()
  for (i in 1:length(by.day)){
    
    samples.by.day <- by.day[[i]]
    combined.by.day <- list()
    
    for (t in 1:dim(samples.by.day)[1]){
      # Get Key
      key <- as.character(samples.by.day$Key[t])
      conditions <- paste(strsplit(key, split="_")[[1]][2:3], collapse="_")
      full.name <- toupper(paste(strsplit(key, split="_")[[1]], collapse=" - "))
      cat("------- Sample", full.name, "-------\n\n")
      
      # Read measurements
      cat("Reading", key, "data ...\n\n")
      sample.data <- CSV.read(as.character(samples.by.day$File[t]))
      # sample.data <- read.table(as.character(samples.by.day$File[t]), sep="\t", header=T)
      sample.data <- sample.data[, c("Wells","Drugs","Controls","Final Conc (nM)","Measurements")]
      # sample.data <- sample.data[, c("Wells","Drugs","Controls","Final.Conc..nM.","Measurements")]
      
      # Annotate drugs by class
      cat("Annotating", key, "drugs by class ...\n\n")
      effect <- rep(NA, dim(sample.data)[1])
      for(e in 1:dim(sample.data)[1]){
        if (!is.na(sample.data$Drug[e])){
          index <- grep(as.character(sample.data$Drug[e]), classes$Name)
          if(length(index) > 0){
            effect[e] <- as.character(classes$Effect[index])
          } else {
            effect[e] <- "Standard Therapy"
          }
        }
      }
      sample.data$Effect <- effect
      sample[[key]] <- sample.data
      
      # Normalize data
      cat("Normalizing", key, "data ...\n\n")
      neg <- mean(sample[[key]]$Measurements[which(sample[[key]]$Controls == neg.name)])
      pos <- mean(sample[[key]]$Measurements[which(sample[[key]]$Controls == pos.name)])
      normalized.name <- "Normalized"
      sample[[key]][, normalized.name] <- ((neg-sample[[key]]$Measurements)/(neg-pos))*100
      
      colnames(sample[[key]])[4] <- "Concentration"
      combined.by.day[[key]] <- sample[[key]]
      
    }
    
    plot.name <- paste(cell.line, names(by.day)[i], sep="_")
    array.object <- plotData(combined.by.day, out.dir, normalized.dir, plot.dir, cell.line, plot.name, array.object$out,
                             array.object$normalized, score.dir, activity.thr)
  }
  
  # Combine all conditions in one plot
  if (combine){
    array.object <- plotData(sample, out.dir, normalized.dir, combined.dir, cell.line, cell.line, array.object$out,
                             array.object$normalized, score.dir, activity.thr)
  }
  

  Array.write(cf, array.object$normalized, 'normalized')
  Array.write(cf, array.object$out, 'out')
  return(0)
}

main(execute)
