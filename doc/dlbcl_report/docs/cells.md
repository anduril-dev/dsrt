# DLBCL cell lines
Cell lines available a the moment at Leppä Lab. Top four rows correspond to the cell lines chosen for the epigenetic screening. The thesis mentioned in the second column is the PhD dissertation by Maria Bach Larusen at Aarhus University: Rituximab Sensitivity in Diffuse Large B-Cell Lymphoma

![Cell lines](img/cell_lines.png)


## SU-DHL-4
* Details:
    * Donor: 38-years-old man (1975).
    * Immunology: CD3 -, CD10 +, CD13 -, CD19 +, CD20 +, CD34 -, CD37 +, CD38 +, cyCD79a +, CD138 -, HLA-DR +, sm/cyIgG +, sm/cyIgM -, sm/cykappa +, sm/cylambda -;
    * Mutations: TODO

## RIVA-I
* Details:
    * Donor: 57-years-old woman at refractory terminal stage (1977).
    * Immunology: CD3 -, CD10 -, CD13 -, CD20 +, CD34 -, CD37 +, CD38 +, CD80 +, HLA-DR +, sm/cyIgG -, sm/cyIgM +, sm/cykappa +, sm/cylambda -;
    * Mutations: TODO

## OCI-LY-19
* Details:
    * Donor: 27-year-old woman at stage 4B, relapse (1987).
    * Immunology: CD3 -, CD10 +, CD13 -, CD19 +, CD20 (+), CD37 -, CD38 +, cyCD79a +, CD80 +, CD138 +, HLA-DR +;
    * Mutations: TODO

## OCI-LY-3
* Details:
    * Donor: 52-year-old man at stage 4B, relapse (1983).
    * Immunology: CD3 -, CD10 -, CD13 -, CD19 -, CD20 +, CD34 -, CD37 +, CD38 +, cyCD79a +, CD80 +, CD138 -, HLA-DR +, sm/cyIgG +, sm/cyIgM -, sm/cykappa -, sm/cylambda +; another source described as CD19 +, cykappa +;
    * Mutations: TODO

# Other DLBCL cell lines
[List with known information from other DLBCL cell lines](https://docs.google.com/spreadsheets/d/1ARIZI7YptTLuwuQpb_Tov40mmTIBdm5qnyFuTX2nj8Q/edit?usp=sharing)



