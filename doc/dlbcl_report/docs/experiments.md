# Experiments List
Main optimization steps for the Epigenetic drug screening

## Cell growth assay
To choose the optimal number of cells per well in each setup, that is, each pre-treatment time, measuring reagent, plate type and volume used per well. Basically, we test eight different dilutions of each cell line using the specifications as guidelines and measure everyday for the number of days set in the experimental design. The aim is to choose the number of cells that is still growing exponentially at the day of the measurement that we will use, and not a number of cells where they started to die.

## Plate survival
Using the number of cells decided in the cell growth assay we test that the read-out on the final day is homogeneous throughout the full plate. This helped on an early stage to filter out cell lines that grew non-uniformly across the plate. This is also done during the Epigenetic drug screening as a dummy plate to have a baseline of the quality of the plate after the whole protocol.

## 2D combination matrix
Using the number of cells decided in the cell growth assay we test seven different dilutions of Rituximab and Doxorubicin to find the optimal combination of the two. These are the two bigger actors in the R-CHOEP therapy.

## Epigenetic drug screening
Pre-coated plates, add media, shake, add cell suspension, for the longest pretreatment time, procedure for cell splitting.

For the epigenetic drug screening, the setup goes as follows:

* Seed cells at fixed concentration chosen in the cell growth assay on pre-coated plates with the compounds at five 10-fold dilutions.
* After pre-treatment time (1 day, 3 days or 9 days) dispense fixed combination of Rituximab and Doxorubicin on the "Treatment" plate, and same volume of PBS on the "Control" plate
* After treatment time (48h chosen based on Rituximab and Doxorubicin literature) measure with CellTiter Glo.

In the case of 9 days of pre-treatment time, every three days we passage the cells and dispense the compounds at the same concentration as in the pre-coated plates.