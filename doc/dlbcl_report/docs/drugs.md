# Compound collection

The collection of compounds to be used was collected through a comprehensive literature search during the Summer 2014. We selected compounds that had been tried in DLBCL or other cancer types

## Original list

References to each drug in a separate Google spreadsheet.

![Compounds0](img/drugs/epi.png)
![Compounds1](img/drugs/other.png)

## Latest additions

only the new ones that were added