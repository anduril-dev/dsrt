# Brief introduction
The standard treatment for Diffuse large B-cell lymphoma, R-CHOEP, usually results in positive outcome. However, almost a third of the patients do not respond to therapy and no alternative treatments are available. This research aims to discover the driver molecules of the resistant phenotype and propose pre-treatments to sensitize the resistant tumors to be treated with R-CHOEP.

# Experimental setup
High-throughput setup to test the reprogramming cabability of each epigenetic compound in a manually curated collection. Cells are treated with the compounds at five doses and their sensitivity to a fixed combination of Rituximab and Doxorubicin is tested at three time points (i.e. Pretreatment time).


![Basic Setup](img/workflow_old.png)

*Basic workflow for the Epigenetic reprogramming screening*

# Preliminary results

![Preliminary Results](img/pie_chart_drugs_findings_old.png)

# Open questions

* TODO
* TODO

# Next steps

* TODO
* TODO