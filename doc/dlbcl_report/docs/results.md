# Table of observed effects

# Reprogramming compounds by cell line

* Drugs where the maximum inhibition is less than 20% in both treatment and control plates are filtered out as inactive.
* Calculate absolute difference between treatment and control to rank drugs by their reprogramming effect
* Filter out drugs with no reprogramming effect (manual threshold).
* Calculate treatment minus control to assess whether the effect is sensitizing or desensitizing.

## SU-DHL-4

![SU-DHL-4](img/results/sudhl4_day1_drug_scores.png)

![SU-DHL-4](img/results/sudhl4_day3_drug_scores.png)

![SU-DHL-4](img/results/sudhl4_day9_drug_scores.png)

## RIVA-I

![RIVA-I](img/results/riva_day1_drug_scores.png)

![RIVA-I](img/results/riva_day3_drug_scores.png)

![RIVA-I](img/results/riva_day9_drug_scores.png)

## OCI-LY-19

![OCI-LY-19](img/results/ocily19_day1_drug_scores.png)

![OCI-LY-19](img/results/ocily19_day3_drug_scores.png)

![OCI-LY-19](img/results/ocily19_day9_drug_scores.png)

## OCI-LY-3

![OCI-LY-3](img/results/ocily3_day1_drug_scores.png)

![OCI-LY-3](img/results/ocily3_day3_drug_scores.png)

![OCI-LY-3](img/results/ocily3_day9_drug_scores.png)